Dynamical plane for complex discrete dynamical system based on [the complex quadratic polynomial](https://en.wikipedia.org/wiki/Complex_quadratic_polynomial) $` f_c(z) = z^2 + c`$

Algorithm:
* [Backward iteration](https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/boettcher#backwards_iteration)



# periodic case

## period 2

Here:
* internal angle  t = 0.49975027919634618290 
* parameter  c = -0.749998153581339 +0.001569040474910*I  on thje boundary of the main cardioid ( period 1 hyperbolic component of the Mandelbrot set)
* [alfa fixed point](https://en.wikipedia.org/wiki/Periodic_points_of_complex_quadratic_mappings#Complex_dynamics) z = a = ( -0.4999993845268604 ; 0.0007845207203064 )  it is a center od Siegel disc 
* period 2 cycle (computed with [mandel](http://www.mndynamics.com/indexp.html) ) zp = { -0.527992812851380  +0.028025773673418*i,  -0.472007187148620  -0.028025773673419*i}
* iteration/turn  = 998 


![6.png](./images/6.png)   

It is made with : 
* [e.c](./src/e.c) - short version: only critical orbit and rays
* [d.c](./src/d.c) - extended version, also Julia set in various algorithms




This is image of dynamical plane with 2 objects: 
* critical orbit ( forward orbit of the critical point z = 0.0) which is also the boundary of the Siegel disc
* 2 external rays for angles 1/3 and 2/3 which spirals around period 2 cycle, and finally land on it 

See also : 
* [commons version](https://commons.wikimedia.org/wiki/File:Siegel_disk_and_external_rays_for_fc(z)_%3D_z*z%2Bc_where_c_%3D_-0.749998153581339_%2B0.001569040474910*I;_t_%3D_0.49975027919634618290.png) with src code 
* 



```c

// dot product 
double dot(complex double a, complex double b){

	// ax*bx + ay*by
	return creal(a)*creal(b) + cimag(a)*cimag(b);
}


int InverseIterationAndDrawSegment (int per, complex double zz[per], unsigned char A[]){
	
	int p;
	int pMax = per;
	complex double z;
	
	
	// compute preimages
	for(p=0; p<pMax; ++p){
		z = zz[(p+1) % per]; //read point from the ray p+1 
		z = csqrt(z-c); // inverse iteration = preimage;  fc(z) maps  z_{l,j} to z_{l-1,j+1}} so f^{-1} map to z_{l+1, j-1}
		
		// choose correct preimage using dot product 
		if (dot(zz[p],z) < 0 ) z = - z; 
		// draw segment of ray p
		DrawClippedLineSegment(zz[p],z, 255, A);
		zz[p] = z; //save 
	}
	
  	return 0; 

}




int DrawExternalDynamicRaysBI (int n, int m, int period, int iMax, unsigned char A[])
{ 
  	int i = 0; // iteration = number of points
  	double r = 10000.0; // very big radius = near infinity where  z=w so one can swith to dynamical plane ( Boettcher conjugation )
  	complex double zz[period]; // zz is an array of z  
  
  	double t;
  	int p;
  	int pMax = period; // number of rays to draw 
  
   
  	// initial points  on pMax rays
  	t = (double)n/m; // first external angle in turns
  	
  	
  	for(p=0; p<pMax; ++p){
  		
  		zz[p] =  r*cexp(2.0*I * M_PI * t ); // Euler's formula gives initial point on the p ray
  		t *= 2.0; // t = 2*t  = angle doubling map ; gives next angle 
  		if (t > 1.0) t--;  // t = t modulo 1 	
      	  	}
  
	for (i = 0; i < iMax; ++i)
    		 
    		InverseIterationAndDrawSegment(period, zz, A); // inverse iteration of  complex quadratic polynomial:  with proper choose of preimage 
      	return 0;
}

```


### distance from the ray to it's' landing point



Distance is getting smaller with each turn  
![distance.png](./images/distance.png)   


but after 979 turns it stops and stay the same. Here is the text output:  


```text
turn = 1 	MinDistanceToLandingPoint =0.1999464067838531
turn = 2 	MinDistanceToLandingPoint =0.0965119366327840
turn = 3 	MinDistanceToLandingPoint =0.0733046877609888
turn = 4 	MinDistanceToLandingPoint =0.0605960873896978
turn = 5 	MinDistanceToLandingPoint =0.0525041058185079
turn = 6 	MinDistanceToLandingPoint =0.0462972945145204
turn = 7 	MinDistanceToLandingPoint =0.0418305708354092
turn = 8 	MinDistanceToLandingPoint =0.0384713175214287
turn = 9 	MinDistanceToLandingPoint =0.0352328752580016
turn = 10 	MinDistanceToLandingPoint =0.0327153421727270
turn = 11 	MinDistanceToLandingPoint =0.0304856605673071
turn = 12 	MinDistanceToLandingPoint =0.0284982010585848
turn = 13 	MinDistanceToLandingPoint =0.0267161485774571
turn = 14 	MinDistanceToLandingPoint =0.0252557444737419
turn = 15 	MinDistanceToLandingPoint =0.0237825297448790
turn = 16 	MinDistanceToLandingPoint =0.0225570510216921
turn = 17 	MinDistanceToLandingPoint =0.0214228478972554
turn = 18 	MinDistanceToLandingPoint =0.0202798458715211
turn = 19 	MinDistanceToLandingPoint =0.0193112243428920
turn = 20 	MinDistanceToLandingPoint =0.0184084574067838
turn = 21 	MinDistanceToLandingPoint =0.0175654632444206
turn = 22 	MinDistanceToLandingPoint =0.0167768489313540
turn = 23 	MinDistanceToLandingPoint =0.0160925636648859
turn = 24 	MinDistanceToLandingPoint =0.0153939819482825
turn = 25 	MinDistanceToLandingPoint =0.0147374621599698
turn = 26 	MinDistanceToLandingPoint =0.0141611689971523
turn = 27 	MinDistanceToLandingPoint =0.0135752436832614
turn = 28 	MinDistanceToLandingPoint =0.0130223652383492
turn = 29 	MinDistanceToLandingPoint =0.0125321982292230
turn = 30 	MinDistanceToLandingPoint =0.0120651288229456
turn = 31 	MinDistanceToLandingPoint =0.0115924766052184
...
turn = 974 	MinDistanceToLandingPoint =0.0000000000000792
turn = 975 	MinDistanceToLandingPoint =0.0000000000000768
turn = 976 	MinDistanceToLandingPoint =0.0000000000000748
turn = 977 	MinDistanceToLandingPoint =0.0000000000000729
turn = 978 	MinDistanceToLandingPoint =0.0000000000000716
turn = 979 	MinDistanceToLandingPoint =0.0000000000000697
turn = 980 	MinDistanceToLandingPoint =0.0000000000000697
turn = 981 	MinDistanceToLandingPoint =0.0000000000000697
turn = 982 	MinDistanceToLandingPoint =0.0000000000000697
turn = 983 	MinDistanceToLandingPoint =0.0000000000000697
turn = 984 	MinDistanceToLandingPoint =0.0000000000000697
turn = 985 	MinDistanceToLandingPoint =0.0000000000000697
turn = 986 	MinDistanceToLandingPoint =0.0000000000000697
```

One can see here with log scale diagram:

![distance_l.png](./images/distance_l.png)   


```gnuplot
set ylabel "distance"
set xlabel "turn"
set title "distance from ray to the landing point after n iteration"
set output "data.png"
unset key
set output "distance.png"
gnuplot> plot "data.txt"
set logscale y
set output "distance_l.png"
gnuplot> plot "data.txt"
```

Here the precision of floating point number is to low. One can increase it with arbitrary precision library, like arb. See example with [repeated square root ](https://gitlab.com/adammajewski/repeated-square-root-) 




### zoom with center at landing points

![radius = 1.0000000000000000](./images/1.0000000000000000.png)  
![radius = 0.1000000000000000](./images/0.1000000000000000.png)  
![radius = 0.0100000000000000](./images/0.0100000000000000.png)  
![radius = 0.0010000000000000](./images/0.0010000000000000.png)  
![radius = 0.0000000000100000](./images/0.0000000000100000.png)  
![radius = 0.0000000000010000](./images/0.0000000000010000.png)  
![radius = 0.0000000000001000](./images/0.0000000000001000.png)  
![radius = 0.0000000000000100](./images/0.0000000000000100.png)  


# Dictionary

## turn
* [turn](https://en.wikipedia.org/wiki/Turn_(angle)) - A turn is a unit of plane angle measurement equal to 2π radians, 360 degrees or 400 gradians
* A turn is also referred to as a cycle (abbreviated cyc), revolution (abbreviated rev), complete rotation (abbreviated rot) or full circle.

Here NoOfTurns is a measure of external ray's 'rotations around it's landing point 

It is measured in funtion InverseIterationAndDrawSegment: 

```c
int InverseIterationAndDrawSegment (int per, complex double zz[per], unsigned char A[]){
	
	int p;
	int pMax = per;
	complex double z;
	complex double zn; // next vsalue : zn = f^(-1)(z)
	
	
	
	// compute preimages
	for(p=0; p<pMax; ++p){
		z = zz[(p+1) % per]; //read point from the ray p+1 
		zn = csqrt(z-c); // inverse iteration = preimage;  fc(z) maps  z_{l,j} to z_{l-1,j+1}} so f^{-1} map to z_{l+1, j-1}
		
		// choose correct preimage using dot product 
		if (dot(zz[p],zn) < 0 ) zn = - zn; 
		// compute number of turns around period 2 points only for one ray; p==0 means t = 1/3 
		if (p==0) {
			if (! bCounted && creal(zn) > z20x ) {
				NoOfTurns +=1; 
				bCounted = 1; 
				MinDistanceToLandingPoint = cabs(zn-z2[0]);
				printf("turn = %d \tMinDistanceToLandingPoint =%.16f\n", NoOfTurns, MinDistanceToLandingPoint); 
				if (cabs(z-zn)  < 3e-16 ) {
					printf("error\n");
					return 1;	}
				}
				
				else if (bCounted && creal(zn) < z20x ) bCounted = 0;
				
		}	
		// draw segment of ray p
		DrawClippedLineSegment(zz[p],zn, 255, A);
		zz[p] = zn; //save 
	}
	
	
		
	
  	return 0; 


}




```

# key words
* complex quadratic polynomial
* external ray
* dynamic plane
* complex number
* discrete local complex dynamics
* spiral
* discrete dynamical system



# License

This project is licensed under the   Apache License Version 2.0, January 2004 - see the [LICENSE](LICENSE) file for details


# git

## commands
```git
  cd existing_folder
  git init
  git remote add origin git@gitlab.com/adammajewski/ray-backward-iteration
  git add .
  git commit -m "descr"
  git push -u origin master
```

also:  

```git
   git config remote.origin.url git@gitlab.com:adammajewski/ray-backward-iteration.git
```

subdirectory:

```git
mkdir images
git add *.png
git mv  *.png ./images
git commit -m "move"
git push -u origin master
```

and link images : 

```txt
![](./images/n.png "description") 
```



local repo: /home/a/c/julia/ext_ray/ray-backward-iteration




## markdown format
[GitLab](https://gitlab.com/) uses:
* the Redcarpet Ruby library for [Markdown processing](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md)
* KaTeX to render [math written with the LaTeX syntax](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md), but [only subset](https://khan.github.io/KaTeX/function-support.html)



