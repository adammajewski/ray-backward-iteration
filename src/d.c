/*

  Adam Majewski
  adammaj1 aaattt o2 dot pl  // o like oxygen not 0 like zero 
  
  
  console program in c programing language 
  
  ==============================================
  
  
  Structure of a program or how to analyze the program 
  
  
  ============== Image X ========================
  
  DrawImageOfX -> DrawPointOfX -> ComputeColorOfX 
  
  first 2 functions are identical for every X
  check only last function =  ComputeColorOfX
  which computes color of one pixel !
  
  

   
  ==========================================

  
  ---------------------------------
  indent d.c 
  default is gnu style 
  -------------------



  c console progam 
  
	export  OMP_DISPLAY_ENV="TRUE"	
  	gcc d.c -lm -Wall -march=native -fopenmp
  	time ./a.out > b.txt


  gcc d.c -lm -Wall -march=native -fopenmp


  time ./a.out

  time ./a.out >d.txt

  ----------------------
  
 real	0m19,809s
user	2m26,763s
sys	0m0,161s



period 2 cycle
computed with mandel
zp
z = -0.527992812851380  +0.028025773673418 i
z = -0.472007187148620  -0.028025773673419 i

  

*/

#include <stdio.h>
#include <stdlib.h>		// malloc
#include <string.h>		// strcat
#include <stdbool.h>
#include <math.h>		// M_PI; needs -lm also
#include <complex.h>
#include <omp.h>		// OpenMP

/* --------------------------------- global variables and consts ------------------------------------------------------------ */



// virtual 2D array and integer ( screen) coordinate
// Indexes of array starts from 0 not 1 
//unsigned int ix, iy; // var
static unsigned int ixMin = 0;	// Indexes of array starts from 0 not 1
static unsigned int ixMax;	//
static unsigned int iWidth;	// horizontal dimension of array

static unsigned int iyMin = 0;	// Indexes of array starts from 0 not 1
static unsigned int iyMax;	//

static unsigned int iHeight = 2002;	//  proportional to size of image file !!!!!!


// The size of array has to be a positive constant integer 
static unsigned int iSize;	// = iWidth*iHeight; 

// memmory 1D array 
unsigned char *data;
unsigned char *edge;
unsigned char *edge2;

// unsigned int i; // var = index of 1D array
//static unsigned int iMin = 0; // Indexes of array starts from 0 not 1
static unsigned int iMax;	// = i2Dsize-1  = 
// The size of array has to be a positive constant integer 
// unsigned int i1Dsize ; // = i2Dsize  = (iMax -iMin + 1) =  ;  1D array with the same size as 2D array


double ZxMin ; //= -1.7; //-2.0;	//-0.05;
double ZxMax ; //=  1.7; // 2.0;	//0.75;
double ZyMin ;// //= -1.7; //-2.0;	//-0.1;
double ZyMax ; // =  1.7; //2.0;	//0.7;



static double PixelWidth;	// =(ZxMax-ZxMin)/ixMax;
static double PixelHeight;	// =(ZyMax-ZyMin)/iyMax;
static double ratio;


// complex numbers of parametr plane 
double complex c;		// parameter of function fc(z)=z^2 + c
double complex a; // alfa fixed point

//int Period = 2;


static unsigned long int iterMax = 1000000;	//iHeight*100;

static double ER = 1E60;		// EscapeRadius for bailout test 
double EscapeRadius=1000000; // = ER big !!!!
// SAC/J
double lnER; // ln(ER)
int i_skip = 2; // exclude (i_skip+1) elements from average
unsigned char s = 7; // stripe density

double BoundaryWidth = 3.0; // % of image width  
double distanceMax; //distanceMax = BoundaryWidth*PixelWidth;

double InternalSiegelDiscRadius; //https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/siegel
double ExternalSiegelDiscRadius;

/* colors = shades of gray from 0 to 255 */
unsigned char iColorOfExterior = 245;
unsigned char iColorOfInterior = 55;
unsigned char iColorOfInterior1 = 210;
unsigned char iColorOfInterior2 = 180;
unsigned char iColorOfBoundary = 0;
unsigned char iColorOfUnknown = 30;



int NoOfExteriorPixels = 0;
int NoOfInteriorPixels = 0;
int NoOfUnknownPixels = 0;


/* ------------------------------------------ functions -------------------------------------------------------------*/


int SetPlane(complex double center, double radius, double AspectRatio){


	ZxMin = creal(center) - radius*AspectRatio; //-2.0;	//-0.05;
  	ZxMax = creal(center) + radius*AspectRatio; // 2.0;	//0.75;
	ZyMin = cimag(center) - radius; //-2.0;	//-0.1;
	ZyMax = cimag(center) + radius; //2.0;	//0.7;


	return 0;

}



/* -----------  array functions = drawing -------------- */

/* gives position of 2D point (ix,iy) in 1D array  ; uses also global variable iWidth */
unsigned int Give_i (unsigned int ix, unsigned int iy)
{
  return ix + iy * iWidth;
}


/* 
   gives position ( index) in 1D virtual array  of 2D point Z 
   without bounds check !!
*/
int Give_i_from_d(complex double Z){ // double version of Give_k
  /* translate from world to screen coordinate */

  //  iY=(ZyMax-Zy)/PixelHeight; /*  */
  int ix=(creal(Z)-ZxMin)/PixelWidth;
  int iy=(ZyMax - cimag(Z))/PixelHeight; /* reverse Y  axis */		

	
  return Give_i(ix,iy);

}

 


void ColorPixel(int iColor, int i, unsigned char A[])
{
  A[i]   = iColor;
  
}




int  ColorPixel_d(complex double z, int iColor,  unsigned char A[]){
  int i = Give_i_from_d(z); // compute index of 1D array
  if ( i<0  || i>iSize) {printf(" bad i from color pixel\n");return 1;}
  ColorPixel(iColor, i, A);
  //printf("plot z = %f;%f ; i = %d\n",creal(z), cimag(z), i);
	
  return 0;

}






// plots raster point (ix,iy) 
int iDrawPoint(unsigned int ix, unsigned int iy, unsigned char iColor, unsigned char A[])
{ 

 /* i =  Give_i(ix,iy) compute index of 1D array from indices of 2D array */
 A[Give_i(ix,iy)] = iColor;

return 0;
}


// draws point to memmory array data
// uses complex type so #include <complex.h> and -lm 
int dDrawPoint(complex double point,unsigned char iColor, unsigned char A[] )
{

  unsigned int ix, iy; // screen coordinate = indices of virtual 2D array
  //unsigned int i; // index of 1D array
  
  ix = (creal(point)- ZxMin)/PixelWidth; 
  iy = (ZyMax - cimag(point))/PixelHeight; // inverse Y axis 
  iDrawPoint(ix, iy, iColor, A);
return 0;
}

/*
http://rosettacode.org/wiki/Bitmap/Bresenham%27s_line_algorithm
Instead of swaps in the initialisation use error calculation for both directions x and y simultaneously:
*/
void iDrawLine( int x0, int y0, int x1, int y1, unsigned char iColor, unsigned char A[]) 
{
  int x=x0; int y=y0;
  int dx = abs(x1-x0), sx = x0<x1 ? 1 : -1;
  int dy = abs(y1-y0), sy = y0<y1 ? 1 : -1; 
  int err = (dx>dy ? dx : -dy)/2, e2;

  for(;;){
    iDrawPoint(x, y, iColor, A);
    if (x==x1 && y==y1) break;
    e2 = err;
    if (e2 >-dx) { err -= dy; x += sx; }
    if (e2 < dy) { err += dx; y += sy; }
  }
}




int dDrawLineSegment(double Zx0, double Zy0, double Zx1, double Zy1, int color, unsigned char *array) 
{

 unsigned int ix0, iy0; // screen coordinate = indices of virtual 2D array 
 unsigned int ix1, iy1; // screen coordinate = indices of virtual 2D array

   // first step of clipping
   //if (  Zx0 < ZxMax &&  Zx0 > ZxMin && Zy0 > ZyMin && Zy0 <ZyMax 
    //  && Zx1 < ZxMax &&  Zx1 > ZxMin && Zy1 > ZyMin && Zy1 <ZyMax )
   ix0= (Zx0- ZxMin)/PixelWidth; 
   iy0 = (ZyMax - Zy0)/PixelHeight; // inverse Y axis 
   ix1= (Zx1- ZxMin)/PixelWidth; 
   iy1= (ZyMax - Zy1)/PixelHeight; // inverse Y axis 
   // second step of clipping
   if (ix0 >=ixMin && ix0<=ixMax && ix0 >=ixMin && ix0<=ixMax && iy0 >=iyMin && iy0<=iyMax 
      && iy1 >=iyMin && iy1<=iyMax )
   iDrawLine(ix0,iy0,ix1,iy1,color, array) ;

return 0;
}







// -------------------------------
// https://en.wikipedia.org/wiki/Cohen%E2%80%93Sutherland_algorithm

typedef int OutCode;

const int INSIDE = 0; // 0000
const int LEFT = 1;   // 0001
const int RIGHT = 2;  // 0010
const int BOTTOM = 4; // 0100
const int TOP = 8;    // 1000

// Compute the bit code for a point (x, y) using the clip rectangle
// bounded diagonally by (ZxMin, ZyMin), and (ZxMax, ZyMax)

// ASSUME THAT ZxMax, ZxMin, ZyMax and ZyMin are global constants.

OutCode ComputeOutCode(double x, double y)
{
	OutCode code;

	code = INSIDE;          // initialised as being inside of [[clip window]]

	if (x < ZxMin)           // to the left of clip window
		code |= LEFT;
	else if (x > ZxMax)      // to the right of clip window
		code |= RIGHT;
	if (y < ZyMin)           // below the clip window
		code |= BOTTOM;
	else if (y > ZyMax)      // above the clip window
		code |= TOP;

	return code;
}

// Cohen–Sutherland clipping algorithm clips a line from
// P0 = (x0, y0) to P1 = (x1, y1) against a rectangle with 
// diagonal from (xmin, ymin) to (xmax, ymax).
// CohenSutherlandLineClipAndDraw
void dDrawLine(double x0, double y0, double x1, double y1,unsigned char iColor, unsigned char A[])
{
	// compute outcodes for P0, P1, and whatever point lies outside the clip rectangle
	OutCode outcode0 = ComputeOutCode(x0, y0);
	OutCode outcode1 = ComputeOutCode(x1, y1);
	bool accept = false;

	while (true) {
		if (!(outcode0 | outcode1)) { // Bitwise OR is 0. Trivially accept and get out of loop
			accept = true;
			break;
		} else if (outcode0 & outcode1) { // Bitwise AND is not 0. (implies both end points are in the same region outside the window). Reject and get out of loop
			break;
		} else {
			// failed both tests, so calculate the line segment to clip
			// from an outside point to an intersection with clip edge
			double x, y;

			// At least one endpoint is outside the clip rectangle; pick it.
			OutCode outcodeOut = outcode0 ? outcode0 : outcode1;

			// Now find the intersection point;
			// use formulas y = y0 + slope * (x - x0), x = x0 + (1 / slope) * (y - y0)
			if (outcodeOut & TOP) {           // point is above the clip rectangle
				x = x0 + (x1 - x0) * (ZyMax - y0) / (y1 - y0);
				y = ZyMax;
			} else if (outcodeOut & BOTTOM) { // point is below the clip rectangle
				x = x0 + (x1 - x0) * (ZyMin - y0) / (y1 - y0);
				y = ZyMin;
			} else if (outcodeOut & RIGHT) {  // point is to the right of clip rectangle
				y = y0 + (y1 - y0) * (ZxMax - x0) / (x1 - x0);
				x = ZxMax;
			} else if (outcodeOut & LEFT) {   // point is to the left of clip rectangle
				y = y0 + (y1 - y0) * (ZxMin - x0) / (x1 - x0);
				x = ZxMin;
			}

			// Now we move outside point to intersection point to clip
			// and get ready for next pass.
			if (outcodeOut == outcode0) {
				x0 = x;
				y0 = y;
				outcode0 = ComputeOutCode(x0, y0);
			} else {
				x1 = x;
				y1 = y;
				outcode1 = ComputeOutCode(x1, y1);
			}
		}
	}
	if (accept) {
			
               // printf(	"x0 = %d, y0 = %d, x1 = %d, y1 =%d \n",x0, y0, x1, y1);			
		dDrawLineSegment(x0, y0, x1, y1, iColor, A);
	}
}

// -----------------------------------------------------------------------






int DrawClippedLineSegment(complex double z0, complex double z1, unsigned char iColor, unsigned char A[]){


	double x0 = creal(z0);
	double y0 = cimag(z0);
	double x1 = creal(z1);
	double y1 = cimag(z1);
	
	dDrawLine( x0, y0, x1, y1, iColor, A);
	return 0; 



}



































//------------------mapping between world and integer coordinate -----------------------------------------------------





// from screen to world coordinate ; linear mapping
// uses global cons
double GiveZx ( int ix)
{
  return (ZxMin + ix * PixelWidth);
}

// uses globaal cons
double GiveZy (int iy) {
  return (ZyMax - iy * PixelHeight);
}				// reverse y axis


complex double GiveZ( int ix, int iy){
  double Zx = GiveZx(ix);
  double Zy = GiveZy(iy);
	
  return Zx + Zy*I;
	
	


}




// ****************** DYNAMICS = trap tests ( target sets) ****************************



// bailout test
// z escapes when 
// abs(z)> ER or cabs2(z)> ER2 
// https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/Julia_set#Boolean_Escape_time

int Escapes(complex double z){
 // here target set (trap) is the exterior  circle with radsius = ER ( EscapeRadius) 
  // with ceter = origin z= 0
  // on the Riemann sphere it is a circle with point at infinity as a center  
   
  if (cabs(z)>ER) return 1;
  return 0;
}





// compute alfa fixed point
// https://en.wikipedia.org/wiki/Periodic_points_of_complex_quadratic_mappings#Period-1_points_(fixed_points)
complex double GiveAlfa(complex double c)
{
	// d=1-4c 
	// alfa = (1-sqrt(d))/2 
	return (1.0-csqrt(1.0 - 4.0*c))/2.0 ;

}


// https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/siegel

double GiveInternalSiegelDiscRadius(complex double c, complex double a)
{ /* compute critical orbit and finds smallest distance from fixed point */
  int i; /* iteration */
  double complex z =0.0; /* critical point */
  
  /* center of Siegel disc  = a */
  
  double d; // distance
  double dMin = 2.0;

  
  for (i=0;i<=40000 ;i++) /* to small number of iMax gives bad result */
    {
      z = z*z + c; 
      /* */
      
     d = cabs(z - a);
     if (d < dMin) dMin = d; /* smallest distance */
    }
    
  return dMin;
}


double GiveExternalSiegelDiscRadius(complex double c, complex double a)
{ /* compute critical orbit and finds smallest distance from fixed point */
  int i; /* iteration */
  double complex z =0.0; /* critical point */
  
  /* center of Siegel disc  = a */
  
  double d; // distance
  double dMax = 0.0;

  
  for (i=0;i<=40000 ;i++) /* to small number of iMax gives bad result */
    {
      z = z*z + c; 
      /* */
      
     d = cabs(z - a);
     if (d > dMax) dMax = d; /* smallest distance */
    }
    
  return dMax;
}

double GiveMaxDistanceFromCenter(complex double z, complex double c, complex double a)
{ /* compute critical orbit and finds smallest distance from fixed point */
  int i; /* iteration */
  
  
  /* center of Siegel disc  = a */
  
  double d; // distance
  double dMax = 0.0;

  
  for (i=0;i<=40000 ;i++) /* to small number of iMax gives bad result */
    {
      z = z*z + c; 
      /* */
      
     d = cabs(z - a);
     if (d > dMax) dMax = d; /* smallest distance */
    }
    
  return dMax;
}



// ***********************************************************************************************
// ********************** edge detection usung Sobel filter ***************************************
// ***************************************************************************************************

// from Source to Destination
int ComputeBoundaries(unsigned char S[], unsigned char D[])
{
 
  unsigned int iX,iY; /* indices of 2D virtual array (image) = integer coordinate */
  unsigned int i; /* index of 1D array  */
  /* sobel filter */
  unsigned char G, Gh, Gv; 
  // boundaries are in D  array ( global var )
 
  // clear D array
  memset(D, iColorOfExterior, iSize*sizeof(*D)); // for heap-allocated arrays, where N is the number of elements = FillArrayWithColor(D , iColorOfExterior);
 
  // printf(" find boundaries in S array using  Sobel filter\n");   
#pragma omp parallel for schedule(dynamic) private(i,iY,iX,Gv,Gh,G) shared(iyMax,ixMax)
  for(iY=1;iY<iyMax-1;++iY){ 
    for(iX=1;iX<ixMax-1;++iX){ 
      Gv= S[Give_i(iX-1,iY+1)] + 2*S[Give_i(iX,iY+1)] + S[Give_i(iX-1,iY+1)] - S[Give_i(iX-1,iY-1)] - 2*S[Give_i(iX-1,iY)] - S[Give_i(iX+1,iY-1)];
      Gh= S[Give_i(iX+1,iY+1)] + 2*S[Give_i(iX+1,iY)] + S[Give_i(iX-1,iY-1)] - S[Give_i(iX+1,iY-1)] - 2*S[Give_i(iX-1,iY)] - S[Give_i(iX-1,iY-1)];
      G = sqrt(Gh*Gh + Gv*Gv);
      i= Give_i(iX,iY); /* compute index of 1D array from indices of 2D array */
      if (G==0) {D[i]=255;} /* background */
      else {D[i]=0;}  /* boundary */
    }
  }
 
   
 
  return 0;
}



// copy from Source to Destination
int CopyBoundaries(unsigned char S[],  unsigned char D[])
{
 
  unsigned int iX,iY; /* indices of 2D virtual array (image) = integer coordinate */
  unsigned int i; /* index of 1D array  */
 
 
  //printf("copy boundaries from S array to D array \n");
  for(iY=1;iY<iyMax-1;++iY)
    for(iX=1;iX<ixMax-1;++iX)
      {i= Give_i(iX,iY); if (S[i]==0) D[i]=0;}
 
 
 
  return 0;
}





// ***************************************************************************************************************************
// ************************** DEM/J*****************************************
// ****************************************************************************************************************************

unsigned char ComputeColorOfDEMJ(complex double z){
// https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/Julia_set#DEM.2FJ


  
  int nMax = iterMax;
  complex double dz = 1.0; //  is first derivative with respect to z.
  double distance;
  double cabsz;
	
  int n;

  for (n=0; n < nMax; n++){ //forward iteration
	
    	if (cabs(z) > 1e60 || cabs(dz)> 1e60) break; // big values 
    	if (cabs(z-a)< PixelWidth) return iColorOfInterior; // falls into finite attractor = interior
  			
    dz = 2.0*z * dz; 
    z = z*z +c ; /* forward iteration : complex quadratic polynomial */ 
  }
  
  
  cabsz = cabs(z);
  distance = 2.0 * cabsz* log(cabsz)/ cabs(dz);
  if (distance <distanceMax) return iColorOfBoundary; // distanceMax = BoundaryWidth*PixelWidth;
  // else
  
  return iColorOfExterior;

 
}



// plots raster point (ix,iy) 
int DrawPointOfDEMJ (unsigned char A[], int ix, int iy)
{
  int i;			/* index of 1D array */
  unsigned char iColor;
  complex double z;


  i = Give_i (ix, iy);		/* compute index of 1D array from indices of 2D array */
  z = GiveZ(ix,iy);
  iColor = ComputeColorOfDEMJ(z);
  A[i] = iColor ;		// interior
  
  return 0;
}




// fill array 
// uses global var :  ...
// scanning complex plane 
int DrawImagerOfDEMJ (unsigned char A[])
{
  unsigned int ix, iy;		// pixel coordinate 

  	//printf("compute image \n");
 	// for all pixels of image 
	#pragma omp parallel for schedule(dynamic) private(ix,iy) shared(A, ixMax , iyMax)
  	for (iy = iyMin; iy <= iyMax; ++iy){
    		printf ("DEM/J  %d from %d \r", iy, iyMax);	//info 
    		for (ix = ixMin; ix <= ixMax; ++ix)
      			DrawPointOfDEMJ(A, ix, iy);	//  
  }

  return 0;
}




// ***************************************************************************************************************************
// ************************** Unknown: boundary and slow dynamics *****************************************
// ****************************************************************************************************************************

unsigned char ComputeColorOfUnknown(complex double z){



  
  int nMax = 20; // very low value
  
  //double cabsz;
	
  int n;

  for (n=0; n < nMax; n++){ //forward iteration
	
    	if (cabs(z) > 10000000000*ER )  return iColorOfExterior; // big values
    	if (cabs(z-a) < (PixelWidth/100)) return iColorOfInterior; // falls into finite attractor = interior
  			
    
    z = z*z +c ; /* forward iteration : complex quadratic polynomial */ 
  }
  
  
  
  
  //printf("found \n");
  return iColorOfUnknown;

 
}



// plots raster point (ix,iy) 
int DrawPointOfUnknown (unsigned char A[], int ix, int iy)
{
  int i;			/* index of 1D array */
  unsigned char iColor;
  complex double z;


  i = Give_i (ix, iy);		/* compute index of 1D array from indices of 2D array */
  z = GiveZ(ix,iy);
  iColor = ComputeColorOfUnknown(z);
  A[i] = iColor ;		// interior
  
  return 0;
}




// fill array 
// uses global var :  ...
// scanning complex plane 
int DrawImagerOfUnknown (unsigned char A[])
{
  unsigned int ix, iy;		// pixel coordinate 

  	printf("compute image of Unknown\n");
 	// for all pixels of image 
	#pragma omp parallel for schedule(dynamic) private(ix,iy) shared(A, ixMax , iyMax)
  	for (iy = iyMin; iy <= iyMax; ++iy){
    		printf ("Unknown %d from %d \r", iy, iyMax);	//info 
    		for (ix = ixMin; ix <= ixMax; ++ix)
      			DrawPointOfUnknown(A, ix, iy);	//  
  }

  return 0;
}





// ***************************************************************************************************************************
// ************************** LSM/J*****************************************
// ****************************************************************************************************************************

unsigned char ComputeColorOfLSM(complex double z){

 	int nMax = iterMax;
  	unsigned char iColor = iColorOfUnknown;
  	double d; 
	int n;

  	for (n=0; n < nMax; n++){ //forward iteration
	
    		if (cabs(z) > 2.0) return 231+ 14*(n % 2); // escaping set = exterior : LSM/J
    		d = cabs(z-a);
    		if (d <= InternalSiegelDiscRadius) // falls into finite attractor = interior
    			{
    				//d = GiveMaxDistanceFromCenter(z,c,a);
    				//return  255-255.0 * d/ExternalSiegelDiscRadius; 
    				return iColorOfInterior;
    			
    			
    			}
    			
  			
   
     	z = z*z +c ; /* forward iteration : complex quadratic polynomial */ 
  }
  
  
  
  
  
  return iColor;


}



// plots raster point (ix,iy) 
int DrawPointOfLSM (unsigned char A[], int ix, int iy)
{
  int i;			/* index of 1D array */
  unsigned char iColor;
  complex double z;


  i = Give_i (ix, iy);		/* compute index of 1D array from indices of 2D array */
  z = GiveZ(ix,iy);
  iColor = ComputeColorOfLSM(z);
  A[i] = iColor ;		// interior
  
  return 0;
}




// fill array 
// uses global var :  ...
// scanning complex plane 
int DrawImagerOfLSM (unsigned char A[])
{
  unsigned int ix, iy;		// pixel coordinate 

  	//printf("compute image \n");
 	// for all pixels of image 
	#pragma omp parallel for schedule(dynamic) private(ix,iy) shared(A, ixMax , iyMax)
  	for (iy = iyMin; iy <= iyMax; ++iy){
    		printf ("LSM  %d from %d \r", iy, iyMax);	//info 
    		for (ix = ixMin; ix <= ixMax; ++ix)
      			DrawPointOfLSM(A, ix, iy);	//  
  }

  return 0;
}




// ***************************************************************************************************************************
// ************************** Interior : orbits*****************************************
// ****************************************************************************************************************************

unsigned char ComputeColorOfOrbits(complex double z){

 	int nMax = iterMax;
  	unsigned char iColor = iColorOfUnknown;
  	double d; 
	int n;

  	for (n=0; n < nMax; n++){ //forward iteration
	
    		if (cabs(z) > 2.0) return iColorOfExterior; // escaping set = exterior : LSM/J
    		d = cabs(z-a);
    		if (d <= InternalSiegelDiscRadius) // falls into finite attractor = interior
    			{
    				d = GiveMaxDistanceFromCenter(z,c,a);
    				return  255-255.0 * d/ExternalSiegelDiscRadius; 
    				//return iColorOfInterior;
    			
    			
    			}
    			
  			
   
     	z = z*z +c ; /* forward iteration : complex quadratic polynomial */ 
  }
  
  
  
  
  
  return iColor;


}



// plots raster point (ix,iy) 
int DrawPointOfOrbits (unsigned char A[], int ix, int iy)
{
  int i;			/* index of 1D array */
  unsigned char iColor;
  complex double z;


  i = Give_i (ix, iy);		/* compute index of 1D array from indices of 2D array */
  z = GiveZ(ix,iy);
  iColor = ComputeColorOfOrbits(z);
  A[i] = iColor ;		// 
  
  return 0;
}




// fill array 
// uses global var :  ...
// scanning complex plane 
int DrawImageOfOrbits (unsigned char A[])
{
  unsigned int ix, iy;		// pixel coordinate 

  	//printf("compute image \n");
 	// for all pixels of image 
	#pragma omp parallel for schedule(dynamic) private(ix,iy) shared(A, ixMax , iyMax)
  	for (iy = iyMin; iy <= iyMax; ++iy){
    		printf ("Orbits  %d from %d \r", iy, iyMax);	//info 
    		for (ix = ixMin; ix <= ixMax; ++ix)
      			DrawPointOfOrbits(A, ix, iy);	//  
  }

  return 0;
}


// ***************************************************************************************************************************
// ************************** binary Escape time *****************************************
// ****************************************************************************************************************************

unsigned char ComputeColorOfBET(complex double z){

 	int nMax = iterMax;
  	//unsigned char iColor = iColorOfUnknown;
  	// double d; 
	int n;

  	for (n=0; n < nMax; n++){ //forward iteration
		// test for the exterior: bailout test 
    		if (cabs(z) > 2.0) {
    			NoOfExteriorPixels += 1;
    			return iColorOfExterior; // escaping set = exterior : LSM/J
    			}
    		
    		
    		// test for interior 
    		if (cabs(z-a) <= InternalSiegelDiscRadius) {// falls into finite attractor = interior
    			NoOfInteriorPixels += 1;
    			return iColorOfInterior;
    			}
    	 	z = z*z +c ; /* forward iteration : complex quadratic polynomial */ 
  	}
  
  
  
  
  	NoOfUnknownPixels += 1;
  	return iColorOfUnknown;


}



// plots raster point (ix,iy) 
int DrawPointOfBET (unsigned char A[], int ix, int iy)
{
  int i;			/* index of 1D array */
  unsigned char iColor;
  complex double z;


  i = Give_i (ix, iy);		/* compute index of 1D array from indices of 2D array */
  z = GiveZ(ix,iy);
  iColor = ComputeColorOfBET(z);
  A[i] = iColor ;		// 
  
  return 0;
}




// fill array 
// uses global var :  ...
// scanning complex plane 
int DrawImageOfBET (unsigned char A[])
{
  unsigned int ix, iy;		// pixel coordinate 
  
  // 
  NoOfExteriorPixels = 0;
  NoOfInteriorPixels = 0;
  NoOfUnknownPixels = 0;

  	//printf("compute image \n");
 	// for all pixels of image 
	#pragma omp parallel for schedule(dynamic) private(ix,iy) shared(A, ixMax , iyMax)
  	for (iy = iyMin; iy <= iyMax; ++iy){
    		printf ("BET  %d from %d \r", iy, iyMax);	//info 
    		for (ix = ixMin; ix <= ixMax; ++ix)
      			DrawPointOfBET(A, ix, iy);	//  
  }

  // local info
  // BET, number of pixels : exterior = 52 809 244 , interior = 13 749 273, unknown = 108 305 , all = 66 666 822
  
  
  printf("BET, number of pixels : exterior = %d , interior = %d, unknown = %d , all = %d \n", NoOfExteriorPixels, NoOfInteriorPixels, NoOfUnknownPixels, NoOfExteriorPixels+NoOfInteriorPixels + NoOfUnknownPixels );
  //
  NoOfExteriorPixels = 0;
  NoOfInteriorPixels = 0;
  NoOfUnknownPixels = 0;
  return 0;
}





// ***************************************************************************************************************************
// ************************** binary decomposition BD/J*****************************************
// ****************************************************************************************************************************

unsigned char ComputeColorOfBD(complex double z){

 int nMax = 255;
  //double cabsz;
  unsigned char iColor;
	
  int n;

  for (n=0; n < nMax; n++){ //forward iteration
	
    	if (cabs(z) > ER) break; // esacping
    	if (cabs(z-a)< PixelWidth) break; // fails into finite attractor = interior
  			
   
     	z = z*z +c ; /* forward iteration : complex quadratic polynomial */ 
  }
  
  if (creal(z)>0.0) 
  	iColor = 255; 
  	else iColor = 0;
  
  
  return iColor;


}



// plots raster point (ix,iy) 
int DrawPointOfBD (unsigned char A[], int ix, int iy)
{
  int i;			/* index of 1D array */
  unsigned char iColor;
  complex double z;


  i = Give_i (ix, iy);		/* compute index of 1D array from indices of 2D array */
  z = GiveZ(ix,iy);
  iColor = ComputeColorOfBD(z);
  A[i] = iColor ;		// interior
  
  return 0;
}




// fill array 
// uses global var :  ...
// scanning complex plane 
int DrawImagerOfBD (unsigned char A[])
{
  unsigned int ix, iy;		// pixel coordinate 

  	//printf("compute image \n");
 	// for all pixels of image 
	#pragma omp parallel for schedule(dynamic) private(ix,iy) shared(A, ixMax , iyMax)
  	for (iy = iyMin; iy <= iyMax; ++iy){
    		printf ("BSD  %d from %d \r", iy, iyMax);	//info 
    		for (ix = ixMin; ix <= ixMax; ++ix)
      			DrawPointOfBD(A, ix, iy);	//  
  }

  return 0;
}





// ***************************************************************************************************************************
// ************************** modified binary decomposition BD/J*****************************************
// ****************************************************************************************************************************

unsigned char ComputeColorOfMBD(complex double z){
// const number of iterations
 int nMax = 7;
  //double cabsz;
  unsigned char iColor;
	
  int n;

  for (n=0; n < nMax; n++){ //forward iteration
	//cabsz = cabs(z);
    	//if (cabsz > ER) break; // esacping
    	//if (cabsz< PixelWidth) break; // falls into finite attractor = interior
  			
   
     	z = z*z +c ; /* forward iteration : complex quadratic polynomial */ 
  }
  
  if (cabs(z) > 2.0)
  	{ // exterior
  		if (creal(z)>0.0) 
  			iColor = 255; 
  			else iColor = 0;
  	}
  	else iColor = iColorOfInterior;
  	
  return iColor;


}



// plots raster point (ix,iy) 
int DrawPointOfMBD (unsigned char A[], int ix, int iy)
{
  int i;			/* index of 1D array */
  unsigned char iColor;
  complex double z;


  i = Give_i (ix, iy);		/* compute index of 1D array from indices of 2D array */
  z = GiveZ(ix,iy);
  iColor = ComputeColorOfMBD(z);
  A[i] = iColor ;		// interior
  
  return 0;
}




// fill array 
// uses global var :  ...
// scanning complex plane 
int DrawImagerOMfBD (unsigned char A[])
{
  unsigned int ix, iy;		// pixel coordinate 

  	//printf("compute image \n");
 	// for all pixels of image 
	#pragma omp parallel for schedule(dynamic) private(ix,iy) shared(A, ixMax , iyMax)
  	for (iy = iyMin; iy <= iyMax; ++iy){
    		printf ("MBD  %d from %d \r", iy, iyMax);	//info 
    		for (ix = ixMin; ix <= ixMax; ++ix)
      			DrawPointOfMBD(A, ix, iy);	//  
  }

  return 0;
}




// ***********************************************************************************************
//*************************************** SAC/J **************************************************
// *****************************************************************************************
// https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/stripeAC
// SAC = Stripe Average Coloring

//

// the addend function
// input : complex number z
// output : double number t 
double Give_t(double complex z){

  return 0.5+0.5*sin(s*carg(z));

}

/*
  input :
  - complex number
  - intege
  output = average
 
*/
double Give_Arg(double complex z , int iMax)
{
  int i=0; // iteration 
   
   
  //double complex Z= 0.0; // initial value for iteration Z0
  double A = 0.0; // A(n)
  double prevA = 0.0; // A(n-1)
  double R; // =radius = cabs(Z)
  double d; // smooth iteration count
  double complex dz = 1.0; // first derivative with respect to z
  double de; // Distance Estimation from DEM/J  
   
    
  // iteration = computing the orbit
  for(i=0;i<iMax;i++)
    { 
    
      dz = 2.0 * z * dz ; 
      z = z*z + c; // https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/qpolynomials
      
      if (i>i_skip) A += Give_t(z); // 
      
      R = cabs(z);
      // if(R > EscapeRadius) break; // exterior of M set
  	if (R > 1e60 || cabs(dz)> 1e60) break; // prevent NAN 	 	
      prevA = A; // save value for interpolation
        
    } // for(i=0
   
   
  if (i == iMax) 
    A = -1.0; // interior 
  else { // exterior
    de = 2 * R * log(R) / cabs(dz);
    if (de < distanceMax) A = FP_ZERO; //  boundary
    else {
      // computing interpolated average
      A /= (i - i_skip) ; // A(n)
      prevA /= (i - i_skip - 1) ; // A(n-1) 
      // smooth iteration count
      d = i + 1 + log(lnER/log(R))/M_LN2;
      d = d - (int)d; // only fractional part = interpolation coefficient
      // linear interpolation
      A = d*A + (1.0-d)*prevA;
     }   
  }
    
  return A;  
}
 
 
 
 
 
unsigned char ComputeColorOfSAC(complex double z){

  unsigned char iColor;
  double arg;
  
   
   
  	arg = Give_Arg( z, 2500); //   N in wiki 
	
   	// color is proportional to arg 
	if (arg < 0.0)
           
		iColor = 0;  // interior                        
    
		else //  
			{if (arg == FP_ZERO) 
     				iColor = 255; // boundary     
        			else iColor = (unsigned char) (255 - 255*arg );// exterior
      			}
      
    
  return iColor;


}



// plots raster point (ix,iy) 
int DrawPointOfSAC (unsigned char A[], int ix, int iy)
{
  int i;			/* index of 1D array */
  unsigned char iColor;
  complex double z;


  i = Give_i (ix, iy);		/* compute index of 1D array from indices of 2D array */
  z = GiveZ(ix,iy);
  iColor = ComputeColorOfSAC(z);
  A[i] = iColor ;		//   
  return 0;
}




// fill array 
// uses global var :  ...
// scanning complex plane 
int DrawImagerOMfSAC (unsigned char A[])
{
  unsigned int ix, iy;		// pixel coordinate 

  	//printf("compute image \n");
 	// for all pixels of image 
	#pragma omp parallel for schedule(dynamic) private(ix,iy) shared(A, ixMax , iyMax)
  	for (iy = iyMin; iy <= iyMax; ++iy){
    		printf ("SAC/J :  %d from %d \r", iy, iyMax);	//info 
    		for (ix = ixMin; ix <= ixMax; ++ix)
      			DrawPointOfSAC(A, ix, iy);	//  
  }

  return 0;
}



 
 
// *************************************************************************************************
// ********************************* critical orbit ************************************************
// *********************************************************************************************


// fill array 
// uses global var :  ...
// scanning complex plane 
int DrawImage_CriticalOrbit (unsigned char A[])
{
  int i = 0; // iteration = number of the point
  int iMax = 10000000;
  complex double z = 0.0;
  
   
	
  
  	for (i = 0; i < iMax; ++i){
  		ColorPixel_d(z, 255, A );	//  draw point and check if point is outside image 
  		z = z*z+c; // forward iteration : complex quadratic polynomial
  }

  return 0;
}

 

 
  
// *************************************************************************************************
// ********************************* external ray ************************************************
// *********************************************************************************************



 
// check if period p of n/d is proper under period doubling map   
  // p times (2*n) mod d  should give n  
 
int TestPeriod(int n, int d, int period){
	int p;
	int pMax = period;
	int nn = n; // termporary value
	
	if (n<=0 || d<=0 || period <= 0) {
		printf(" input should be positive \n");
		return 1;
	
	}
	
	for(p=0; p<pMax; p++) 
		nn *=2;
	nn = nn % d; // remainder 
	if (nn-n ==0) 
		return 1; // true
	return 0; // false



}
 
 


// dot product 

double dot(complex double a, complex double b){

	// ax*bx + ay*by
	return creal(a)*creal(b) + cimag(a)*cimag(b);


}


int InverseIterationAndDrawSegment (int per, complex double zz[per], unsigned char A[]){
	
	int p;
	int pMax = per;
	complex double z;
	
	
	// compute preimages
	for(p=0; p<pMax; ++p){
		z = zz[(p+1) % per]; //read point from the ray p+1 
		z = csqrt(z-c); // inverse iteration = preimage;  fc(z) maps  z_{l,j} to z_{l-1,j+1}} so f^{-1} map to z_{l+1, j-1}
		
		// choose correct preimage using dot product 
		if (dot(zz[p],z) < 0 ) z = - z; 
		// draw segment of ray p
		DrawClippedLineSegment(zz[p],z, 255, A);
		zz[p] = z; //save 
	}
	
	
		
	
  	return 0; 


}







// uses global var :  ...
/*
"In the dynamic plane, external rays can be drawn by backwards iteration. It is most effective for a periodic or preperiodic angle.

You must keep track of points on the finite collection of rays with angles phi ,2phi ,4phi ...

Say z_{l,j} corresponds to a radius  R^{1/(2l)}  and the angle  2j*phi .

Then  fc(z) maps  z_{l,j} to z_{l-1,j+1}}

This point, which was constructed before, has two preimages under {\displaystyle f_{c}(z)} {\displaystyle f_{c}(z)} .

The one that is closer to {\displaystyle z_{l-1,j}} {\displaystyle z_{l-1,j}} is the correct one. 
This criterion was proved by Thierry Bousch. The ray will look better when you introduce intermediate points." Wolf Jung


this procedure works only for periodic angles 

*/ 
int DrawExternalDynamicRaysBI (int n, int m, int period, int iMax, unsigned char A[])
{ // In the dynamic plane, external rays can be drawn by backwards iteration. This procedure is only for the periodic angle
  // https://commons.wikimedia.org/wiki/File:Backward_Iteration.svg
  int i = 0; // iteration = number of points
  //int iMax = 10000000;
  double r = 10000.0; // very big radius = near infinity where  z=w so one can swith to dynamical plane ( Boettcher conjugation )
  complex double zz[period]; // zz is an array of z  
  
  double t;
  int p;
  int pMax = period; // number of rays to draw 
  
  // check if period is proper 
  // p times (2*n) mod m  should give n 
  if (TestPeriod(n, m, period)!=1 ) {
  	printf("bad input to the TestPeriod from external ray : error \n");
  	return 1;
  }
  // to do 
  // find landing point  : periodic point  
  // compute how many times ray rotates around it's landing point 
  // compute distance to tha landing point of last point of the ray ( along the ray ? = infinity  so to the circle with fixed radius around landing point) 
  
  
  
  
  
  // initial points  on rays
  t = (double)n/m; // first external angle in turns	
  for(p=0; p<pMax; ++p){
  	// initial point
  	zz[p] =  r*cexp(2.0*I * M_PI * t ); // Euler's formula
  	// next angle 
  	t *= 2.0; // t = 2*t angle doubling map
      	if (t > 1.0) t--;  // t = t modulo 1 
  
  
  }
  
  
	for (i = 0; i < iMax; ++i)
    		// inverse iteration of  complex quadratic polynomial:  z = csqrt(z-c)  with proper choose of preimage for one point on every ray 
    		InverseIterationAndDrawSegment(period, zz, A);
  			

  

  return 0;
}

 
 
 
 
 








// *******************************************************************************************
// ********************************** save A array to pgm file ****************************
// *********************************************************************************************

int SaveArray2PGMFile( unsigned char A[], double k, char* comment )
{
  
  FILE * fp;
  const unsigned int MaxColorComponentValue=255; /* color component is coded from 0 to 255 ;  it is 8 bit color file */
  char name [100]; /* name of file */
  snprintf(name, sizeof name, "%.1f", k); /*  */
  char *filename =strncat(name,".pgm", 4);
  
  
  
  // save image to the pgm file 
  fp= fopen(filename,"wb"); // create new file,give it a name and open it in binary mode 
  fprintf(fp,"P5\n # %s\n %u %u\n %u\n", comment, iWidth, iHeight, MaxColorComponentValue);  // write header to the file
  fwrite(A,iSize,1,fp);  // write array with image data bytes to the file in one step 
  fclose(fp); 
  
  // info 
  printf("File %s saved ", filename);
  if (comment == NULL || strlen(comment) ==0)  
    printf("\n");
  else printf (". Comment = %s \n", comment); 

  return 0;
}







int PrintInfoAboutProgam()
{

  
  // display info messages
  printf ("\n\nNumerical approximation of Julia set for fc(z)= z^2 + c \n");
  //printf ("iPeriodParent = %d \n", iPeriodParent);
  //printf ("iPeriodOfChild  = %d \n", iPeriodChild);
  printf ("parameter c = ( %.16f ; %.16f ) \n", creal(c), cimag(c));
  printf ("fixed point alfa z = a = ( %.16f ; %.16f ) \n", creal(a), cimag(a));
  
  printf ("iSize = %d\n", iSize);
  printf ("Image Width = %f in world coordinate\n", ZxMax - ZxMin);
  // https://en.wikipedia.org/wiki/Aspect_ratio_(image)
  printf ("Image aspect ratio = %.16f\n", ratio);
  printf ("PixelWidth = %f \n", PixelWidth);
  
  printf ("InternalSiegelDiscRadius = %.16f \n", InternalSiegelDiscRadius);
  printf ("ExternalSiegelDiscRadius = %.16f \n", ExternalSiegelDiscRadius);
  
  
  printf("for DEM/J \n");
  if ( distanceMax<0.0 || distanceMax > ER ) printf("bad distanceMax\n");
	printf("Max distance from exterior to the boundary =  distanceMax = %.16f = %f pixels\n",  distanceMax, BoundaryWidth); 
  
  // image corners in world coordinate
  // center and radius
  // center and zoom
  // GradientRepetition
  printf ("Maximal number of iterations = iterMax = %ld \n", iterMax);
  printf ("ratio of image  = %f ; it should be 1.000 ...\n", ratio);
  //
  printf("gcc version: %d.%d.%d\n",__GNUC__,__GNUC_MINOR__,__GNUC_PATCHLEVEL__); // https://stackoverflow.com/questions/20389193/how-do-i-check-my-gcc-c-compiler-version-for-my-eclipse
  // OpenMP version is diplayed in the console 
  return 0;
}





int PrintInfoAboutPoint(complex double z){

	//unsigned int ix, iy;		// pixel coordinate
	// to do 
	
	double arg;
	unsigned char iColor;
	
	arg = Give_Arg( z, 2500); //   N in wiki
	iColor = ComputeColorOfSAC(z);
	
	printf ("parameter z = ( %.16f ; %.16f ) \n", creal(z), cimag(z));
	printf ("SAC/J : arg = %.16f ; iColor = %d  \n", arg, iColor);
	
	

	return z; 

}


// *****************************************************************************
//;;;;;;;;;;;;;;;;;;;;;;  setup ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
// **************************************************************************************

int setup ()
{

  printf ("setup start ");
  c = -0.749998153581339 +0.001569040474910*I; //    t = 0.49975027919634618290 
  a = GiveAlfa(c);
  
  
  
	
  /* 2D array ranges */
  
  iWidth = iHeight;
  iSize = iWidth * iHeight;	// size = number of points in array 
  // iy
  iyMax = iHeight - 1;		// Indexes of array starts from 0 not 1 so the highest elements of an array is = array_name[size-1].
  //ix

  ixMax = iWidth - 1;

  /* 1D array ranges */
  // i1Dsize = i2Dsize; // 1D array with the same size as 2D array
  iMax = iSize - 1;		// Indexes of array starts from 0 not 1 so the highest elements of an array is = array_name[size-1].
  
  ratio = (double) iWidth / iHeight;
  
  SetPlane(a , 0.7, ratio);
  

  /* Pixel sizes */
  PixelWidth = (ZxMax - ZxMin) / ixMax;	//  ixMax = (iWidth-1)  step between pixels in world coordinate 
  PixelHeight = (ZyMax - ZyMin) / iyMax;
  
  
  //((ZxMax - ZxMin) / (ZyMax - ZyMin)) / ((double) iWidth / (double) iHeight);	// it should be 1.000 ...
	
   
	
  
  //ER2 = ER * ER; // for numerical optimisation in iteration
  lnER = log(EscapeRadius); // ln(ER) 
  
   	
  /* create dynamic 1D arrays for colors ( shades of gray ) */
  data = malloc (iSize * sizeof (unsigned char));
  edge = malloc (iSize * sizeof (unsigned char));
  edge2 = malloc (iSize * sizeof (unsigned char));
  	
  if (data == NULL || edge == NULL || edge2 == NULL){
    fprintf (stderr, " Could not allocate memory");
    return 1;
  }

  
 	
  
  BoundaryWidth = 6.0*iWidth/2000.0  ; //  measured in pixels ( when iWidth = 2000) ; such function is stable when iWidth is changing
  distanceMax = BoundaryWidth*PixelWidth; // distance to the boundary from exterior
  
  InternalSiegelDiscRadius = GiveInternalSiegelDiscRadius(c,a);
  ExternalSiegelDiscRadius = GiveExternalSiegelDiscRadius(c,a);
  
  
  printf ("and end\n");
	
  return 0;

} // ;;;;;;;;;;;;;;;;;;;;;;;;; end of the setup ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




int end(){


  printf (" allways free memory (deallocate )  to avoid memory leaks \n"); // https://en.wikipedia.org/wiki/C_dynamic_memory_allocation
  free (data);
  free(edge);
  free(edge2);
  PrintInfoAboutProgam();
  return 0;

}

// ********************************************************************************************************************
/* -----------------------------------------  main   -------------------------------------------------------------*/
// ********************************************************************************************************************

int main () {
  setup ();
  
  
   
  /*
  DrawImagerOfDEMJ(data);
  SaveArray2PGMFile (data, iWidth+0.1, "boundary using DEM/J");
  
  
  
  DrawImagerOfBD(data);
  SaveArray2PGMFile (data, iWidth+0.2, "BD/J");
  
  ComputeBoundaries(data, edge);
  SaveArray2PGMFile (edge, iWidth+0.3, "boundaries of BD/J");
  
  DrawImagerOMfBD(data);
  SaveArray2PGMFile (data, iWidth+0.4, "MBD/J");
  
  ComputeBoundaries(data, edge2);
  SaveArray2PGMFile (edge2, iWidth+0.5, "boundaries of MBD/J");
  
  DrawImagerOfLSM(data);
  SaveArray2PGMFile (data, iWidth+0.6, "LSM/J");
  
  ComputeBoundaries(data, edge);
  SaveArray2PGMFile (edge, iWidth+0.7, "boundaries of LSM/J");
  */
  //DrawImageOfOrbits(data);
  //SaveArray2PGMFile (data, iWidth+0.8, "Orbits");
  
  //CopyBoundaries(edge, data);
  //SaveArray2PGMFile (data, iWidth+0.9, "orbits + boundaries of LSM/J ");
  
 // DrawImageOfBET(data);
  //SaveArray2PGMFile (data, iWidth+1.0, "BET = binary escape time");
  /*
  CopyBoundaries(edge, data);
  SaveArray2PGMFile (data, iWidth+1.1, "BET + boundaries of LSM/J ");
  
  
  CopyBoundaries(edge, edge2);
  SaveArray2PGMFile (edge2, iWidth+1.2, "boundaries of LSM/J and MBD");
  
  
  DrawImagerOfUnknown(data);
  SaveArray2PGMFile (data, iWidth+1.3, "Unknown : boundary and slow dynamics");
  
  
  DrawImagerOMfSAC(data);
  SaveArray2PGMFile (data, iWidth+1.4, "SAC/J + DEM/J");
  */
  
  DrawImage_CriticalOrbit(data);
  SaveArray2PGMFile (data, iWidth+1.5, "critical orbit");
  
  DrawExternalDynamicRaysBI(1,3,2,10000000, data);
  SaveArray2PGMFile (data, iWidth+1.6, "critical orbit and rays");
  
  
  //PrintInfoAboutPoint(ZxMin+ZyMax*I);
  
  end();

  return 0;
}
