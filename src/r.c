/*



--------- gcc-------------------

 gcc r.c -lm -Wall -march=native 
 time ./a.out >r.txt



-------------theory -------------------------- 

https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/boettcher#backwards_iteration



critical component = component including critical point zcr = 0.0
critical component is bounded by rays:  0 and iPeriodChild-1 
 
last componet = component  to the left of critical component
this component is almost not changing when iPeriodChild is increasing , see 

https://commons.wikimedia.org/wiki/File:Parabolic_rays_landing_on_fixed_point.ogv

compute points of 2 external rays bounding last component near alfa 


 // points defining target sets 
  ComputeRays( iPeriodChild, iterMax, c, Za, PixelWidth, zz_rays ) ;
  Zlx = creal(zz_rays[iPeriodChild-2]);   
  Zly = cimag(zz_rays[iPeriodChild-2]);
  Zrx = creal(zz_rays[iPeriodChild-1]); 
  Zry = cimag(zz_rays[iPeriodChild-1]);


---------------optimisation ---------------

On rays : 
* max distanca = 200 // skip first points far away from alfa 
* min distance = 100 // skip points near alfa because it is hard to do ( time consuming ) 
* distance step =10  // use not all but every 10-nth point 

so only 2*(200-100)/10 = 20 points are saved  

--------------- git -----------------------
cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/ray-backward-iteration.git
git add r.c
git commit -m ""
git push -u origin master

------------ gnuplot -----------

ad line 
# x      y 
to a.txt file

gnuplot
set terminal png
set output "a.png"
plot "a.txt"


*/

#define _GNU_SOURCE // asprintf
#include <stdio.h>
#include <stdlib.h> // malloc lldiv
#include <string.h> // strcat
#include <math.h> // M_PI; needs -lm also 
#include <complex.h>

/* --------------------------------- global variables and consts ------------------------------------------------------------ */
#define iPeriodChild 4 // iPeriodChild of secondary component joined by root point
#define iPolygonHalfLength 100 // 
#define iPolygonLength (2*iPolygonHalfLength +1) // polygon will be described by iPolygonLength  =  alfa + 2*iPolygonHalfLength (points of 2 rays)
#define iPolygonNMax (iPolygonLength-1) 
#define iOrbitLength 10000  // bigger = killed !!!


double DistanceMin = 1.0;
double DistanceMax = 500.0;
double DistanceStep = 1.0; // minimal distance between point z_i and z_(i+iPeriodChild)
long long int IterMax = 1000000000000;


complex double c ; // parameter of function fc(z) = z^2+c
// fixed point alfa = z : f(z) = z
complex double alfa ;
double alfaX ;
double alfaY;
complex double zcrp = 0.0; // critical point z 

   
// svg viewport
 int iWidth  = 1000; // pixels
 int iHeight = 1000; // pixels
 int iHalfWidth;
 int iHalfHeight; 
 double PixelX;
 double PixelY;
 double PixelWidth ;


// rectangle of complex plane 
 double xMax ;
 double yMax;
 double xMin ;
 double yMin ;
 



// - --------------------- functions ---------------------------------------------------------------




/* find c in component of Mandelbrot set 
 
   uses code by Wolf Jung from program Mandel
   see function mndlbrot::bifurcate from mandelbrot.cpp
   http://www.mndynamics.com/indexp.html

*/
double complex GiveC(double InternalAngleInTurns, double InternalRadius, unsigned int Period)
{
  //0 <= InternalRay<= 1
  //0 <= InternalAngleInTurns <=1
  double t = InternalAngleInTurns *2*M_PI; // from turns to radians
  double R2 = InternalRadius * InternalRadius;
  double Cx, Cy; /* C = Cx+Cy*i */

  switch ( Period ) // of component 
    {
    case 1: // main cardioid
      Cx = (cos(t)*InternalRadius)/2-(cos(2*t)*R2)/4; 
      Cy = (sin(t)*InternalRadius)/2-(sin(2*t)*R2)/4; 
      break;
    case 2: // only one component 
      Cx = InternalRadius * 0.25*cos(t) - 1.0;
      Cy = InternalRadius * 0.25*sin(t); 
      break;
      // for each iPeriodChild  there are 2^(iPeriodChild-1) roots. 
    default: // higher periods : to do, use newton method 
      Cx = 0.0;
      Cy = 0.0; 
      break; }

  return Cx + Cy*I;
}


/*

  http://en.wikipedia.org/wiki/Periodic_points_of_complex_quadratic_mappings
  z^2 + c = z
  z^2 - z + c = 0
  ax^2 +bx + c =0 // ge3neral for  of quadratic equation
  so :
  a=1
  b =-1
  c = c
  so :

  The discriminant is the  d=b^2- 4ac 

  d=1-4c = dx+dy*i
  r(d)=sqrt(dx^2 + dy^2)
  sqrt(d) = sqrt((r+dx)/2)+-sqrt((r-dx)/2)*i = sx +- sy*i

  x1=(1+sqrt(d))/2 = beta = (1+sx+sy*i)/2

  x2=(1-sqrt(d))/2 = alfa = (1-sx -sy*i)/2

  alfa : attracting when c is in main cardioid of Mandelbrot set, then it is in interior of Filled-in Julia set, 
  it means belongs to Fatou set ( strictly to basin of attraction of finite fixed point )

*/
// uses global variables : 
//  ax, ay (output = alfa(c)) 
double complex GiveAlfaFixedPoint(double complex c)
{
  double dx, dy; //The discriminant is the  d=b^2- 4ac = dx+dy*i
  double r; // r(d)=sqrt(dx^2 + dy^2)
  double sx, sy; // s = sqrt(d) = sqrt((r+dx)/2)+-sqrt((r-dx)/2)*i = sx + sy*i
  double ax, ay;
 
  // d=1-4c = dx+dy*i
  dx = 1 - 4*creal(c);
  dy = -4 * cimag(c);
  // r(d)=sqrt(dx^2 + dy^2)
  r = sqrt(dx*dx + dy*dy);
  //sqrt(d) = s =sx +sy*i
  sx = sqrt((r+dx)/2);
  sy = sqrt((r-dx)/2);
  // alfa = ax +ay*i = (1-sqrt(d))/2 = (1-sx + sy*i)/2
  ax = 0.5 - sx/2.0;
  ay =  sy/2.0;
 

  return ax+ay*I;
}






/* 
   principal square  root of complex number 
   http://en.wikipedia.org/wiki/Square_root

   z1= I;
   z2 = root(z1);
   printf("zx  = %f \n", creal(z2));
   printf("zy  = %f \n", cimag(z2));
*/
double complex root(double x, double y)
{ 
  
  double u;
  double v;
  double r = sqrt(x*x + y*y); 
  
  v = sqrt(0.5*(r - x));
  if (y < 0) v = -v; 
  u = sqrt(0.5*(r + x));
  return u + v*I;
}






double GiveDistance(double z1x, double z1y, double z2x, double z2y){
  

  double dx, dy;
  dx = z1x-z2x;
  dy = z1y-z2y; 
  return sqrt(dx*dx+dy*dy);
}

double GiveDistanceInPixels(double z1x, double z1y, double z2x, double z2y, double PixelSize){
  

    return GiveDistance( z1x, z1y, z2x, z2y)/PixelSize;
}




// This function only works for periodic  angles.
// You must know the iPeriodChild n before calling this function.
// draws all "iPeriodChild" external rays 
// http://commons.wikimedia.org/wiki/File:Backward_Iteration.svg
// based on the code by Wolf Jung from program Mandel
// http://www.mndynamics.com/
// to do : near alf distnace between points of the ray 
// is very small < PixelSize
// 


int ComputeRays( //unsigned char A[],
			    int n, //iPeriodChild of ray's angle under doubling map
			    long long int iterMax,
                            complex double c, 
                            complex double alfa,    
                            complex double zz[iPolygonLength] // output array

			    )
{

  double Cx = creal(c); 
  double Cy = cimag(c);
  // add alfa to the output table, in the middle  
  zz[iPolygonHalfLength] = alfa;


  double complex zPrev;
  double u,v; // zPrev = u+v*I

  double xNew; // new point of the ray
  double yNew;
  
  const double R = 10000; // very big radius = near infinity
  int j; // number of ray 
  long long int iter; // index of backward iteration
  int i=0; // index of the output array

  double t,t0; // external angle in turns 
  double num, den; // t = num / den
  
  
  // optimisation of output
 // complex double zPixel ; // choose z such distance( z1, z2)>= Pixel size
  double zPixelX, zPixelY; 
  double distance;
  
  /* dynamic 1D arrays for coordinates ( x, y) of points with the same R on preperiodic and periodic rays  */
  double *RayXs, *RayYs;
  int iLength = n+2; // length of arrays ?? why +2



  //  creates arrays :  RayXs and RayYs  and checks if it was done
  RayXs = malloc( iLength * sizeof(double) );
  RayYs = malloc( iLength * sizeof(double) );
  if (RayXs == NULL || RayYs==NULL)
    {
      fprintf(stderr,"Could not allocate memory");
      getchar(); 
      return 1; // error
    }
  




   // external angle of the first ray 
   num = 1.0;
   den = pow(2.0,n) -1.0;
   t0 = num/den; // http://fraktal.republika.pl/mset_external_ray_m.html
   t=t0;
   


  //  starting points on periodic rays 
  //  with angles t, 2t, 4t...  and the same radius R
  for (j = 0; j < n; j++)
    { // z= R*exp(2*Pi*t)
      RayXs[j] = R*cos((2*M_PI)*t); 
      RayYs[j] = R*sin((2*M_PI)*t);

      // optimisation of output : check only one ray
      //if (j==iPeriodChild-1)  {zPixelX = RayXs[j] ; zPixelY =  RayYs[j] ; }
      if (j==iPeriodChild-2)  {zPixelX = RayXs[j] ; zPixelY =  RayYs[j] ; }
      //
      num *= 2.0;
      t *= 2.0; // t = 2*t
      if (t > 1.0) t--; // t = t modulo 1
      
    }
  



  // z[k] is n-periodic. So it can be defined here explicitly as well.
  RayXs[n] = RayXs[0]; 
  RayYs[n] = RayYs[0];


  //
  i =0; // skip first point of the ray 

  //   backward iteration of each point z
  for (iter = 0; iter < iterMax; iter++)
   {
      for (j = 0; j < n; j++) // ray number
	{ // u+v*i = sqrt(z-c)   backward iteration in fc plane 
	  zPrev = root(RayXs[j+1] - Cx , RayYs[j+1] - Cy ); // , u, v
	  u=creal(zPrev);
	  v=cimag(zPrev);
                
	  // choose one of 2 roots: u+v*i or -u-v*i
	  if (u*RayXs[j] + v*RayYs[j] > 0) 
	    { xNew = u; yNew = v; } // u+v*i
	  else { xNew = -u; yNew = -v; } // -u-v*i
                

	   


          
          // optimised output 
          if (j==iPeriodChild-2 ) // first ray
          { // printf("%d \t %.0f \n", iter , GiveDistanceInPixels(RayXs[j]+RayYs[j]*I, alfa); // distance to alfa
            //printf("%d \t %.0f \n", iter , GiveDistanceInPixels(RayXs[j]+RayYs[j]*I, xNew+yNew*I)); // distance between points
            distance =  GiveDistanceInPixels(zPixelX, zPixelY, xNew, yNew, PixelWidth);
            if ( distance>DistanceStep  )  // optimisation of output // 
             {  zPixelX = xNew;
                zPixelY = yNew;
                printf("%lld \t %7.0f \t %5.0f \t %f %f \n", iter , distance, GiveDistanceInPixels(zPixelX, zPixelY, alfaX, alfaY, PixelWidth), xNew, yNew); // distance between points
                zz[i] = zPixelX + zPixelY * I; // save to the output array
                
               }
               
          }
          // save also point of the next ray
          if (j==iPeriodChild-1 &&  distance>DistanceStep ) {
                  zz[iPolygonNMax - i] = xNew + yNew * I; // save to the ou
                  if ( i<iPolygonHalfLength-1 )  
                      i +=1; // go to the next point 
                      // or skip 
                     else {printf(" warning 1 : i >=iPolygonHalfLength-1; increase iPolygonHalfLengthLength or decrease iterMax \n"); free(RayXs); free(RayYs); return 1; } // bounds check
               
                 } //  if (j==iPeriodChild-1        

                 
	  
	  //  
	  RayXs[j] = xNew; 
          RayYs[j] = yNew;
          
       
          
                
	} // for j ...

          //RayYs[n+k] cannot be constructed as a preimage of RayYs[n+k+1]
      RayXs[n] = RayXs[0]; 
      RayYs[n] = RayYs[0];
          
       
    }  // for iter

   // free memmory
  free(RayXs);
  free(RayYs);


  //  working
  if ( i<iPolygonHalfLength-1 )  
                      printf(" warning 2 : last i >=iPolygonHalfLength-1; decrease iPolygonHalfLengthLength or increase iterMax \n");
  

  

  return  0; //  
}



int ComputeOrbit(complex double z, complex double c, complex double orbit[iOrbitLength]){

  long long int i;
  
   printf("\n\n\n critical orbit \n");
   for (i = 0; i < iOrbitLength; i++) {
     
     
     orbit[i] = z;
     //printf("z = %f ; %f \n", creal(z), cimag(z) );
     z= z*z+c; // next z
     


   }
    

  

 return 0;
}


// give x coord
int gx(double x){
 
 double t;

 if (fabs(x-alfaX)<PixelWidth) t= iHalfWidth; // alfa in the center
  else t = iHalfWidth+ (x-alfaX)*PixelX;
  return (int) round(t);

}

// give y coord
int gy(double y){
  
 double t; 

 if (fabs(y-alfaY)<PixelWidth) t=iHalfHeight; // alfa in the center
 else t =  iHalfHeight- (y-alfaY)*PixelY; // reverse y axis

 return (int) round(t);

}





// ---------- svg ------------------------------------------------
// http://www.rockini.name


int MakeSVG(int n,  char *sPoints, char *sCircles, complex double fixed, complex double critical, complex double last1, complex double last2){


 

 //printf(" xMax = %f ; xMin = %f, yMax = %f , yMin = %f \n ", xMax, xMin, yMax, yMin);        

 // http://www.december.com/html/spec/color4.html  
char *black    = "#000000"; /* hexadecimal number as a string for svg color*/
//char *white    = "#FFFFFF";
//char *burgundy = "#9E0508";
char *SeaGreen = "#068481";
//char *turquoise= "#40E0D0";
char *red      = "#FF0000";
//char *navy     = "#000080";
 char *blue     = "#0000FF";

	 	 

 FILE * fp;
 char *filename;
 char name [100]; /* name of file */
 char *comment = "<!-- sample comment in SVG file  \n can be multi-line -->";
 // char points[LENGTH*8]; // length * sizeof(double) to do 
 //int i;
 

 snprintf(name, sizeof name, "%d", n ); /*  */
  filename =strncat(name,".svg", 4);
  

 fp = fopen( filename,"w");
 if( fp == NULL ) {printf (" file open error \n"); return 1; }


 // viewBox=\"%f %f %f %f //  xMin, yMin, xMax - xMin, (yMax - yMin)
 fprintf(fp,
     "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
     "%s \n"
     "<svg width=\"%d\" height=\"%d\" \n"
     " xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">\n",
     comment, iWidth, iHeight);
  //  printf(" begin done \n");

 
 // mark polygon = 2 external rays and fixed point alfa 
 // polygon <polygon points="10,0  60,0  35,50" style="stroke:#660000; fill:#cc3333;"/>
   fprintf(fp,"<polygon points=\"%s\" style=\"fill: #FFEBCD;stroke:black;stroke-width:1\"/>\n", sPoints);

 // mark a group of points
  fprintf(fp,"<g stroke=\"black\" fill=\"black\" stroke-width=\"1\"  >\n");
   // <circle cx="25" cy="25" r="15" />
  // for (i = 0; i < iOrbitLength; i++)
   //   fprintf(fp,"<circle cx=\"%f\" cy=\"%f\" r=\"%f\"/>\n", creal(orbit[i]), -cimag(orbit[i]),  r);
  fprintf(fp,"%s", sCircles);
  fprintf(fp,"</g>\n");

 // lines <line x1="0" y1="0" x2="200" y2="200" style="stroke:rgb(255,0,0);stroke-width:2" />
 fprintf(fp,"<line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" style=\"stroke:%s;stroke-width:2\" />\n",
    gx(creal(last2)), gy(cimag(last2)),  gx(alfaX), gy(alfaY), SeaGreen); 
  
 fprintf(fp,"<line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" style=\"stroke:%s;stroke-width:2\" />\n",
    gx(creal(last1)), gy(cimag(last1)),  gx(alfaX), gy(alfaY), SeaGreen);

 

 // mark important single points 
 fprintf(fp,"<circle cx=\"%d\" cy=\"%d\" r=\"%d\" style=\"stroke:%s; stroke-width:1; fill:%s\" opacity=\"0.7\"/>\n",
   gx(alfaX), gy(alfaY), iWidth/100 , black, red);

 fprintf(fp,"<circle cx=\"%d\" cy=\"%d\" r=\"%d\" style=\"stroke:%s; stroke-width:1; fill:%s\" opacity=\"0.7\"/>\n",
    gx(0.0), gy(0.0), iWidth/100 , black, blue);
  
 fprintf(fp,"<circle cx=\"%d\" cy=\"%d\" r=\"%d\" style=\"stroke:%s; stroke-width:1; fill:%s\" opacity=\"0.7\"/>\n",
    gx(creal(last1)), gy(cimag(last1)), iWidth/200 , black, SeaGreen);  

 fprintf(fp,"<circle cx=\"%d\" cy=\"%d\" r=\"%d\" style=\"stroke:%s; stroke-width:1; fill:%s\" opacity=\"0.7\"/>\n",
    gx(creal(last2)), gy(cimag(last2)), iWidth/200 , black, SeaGreen); 


 

// end svg 
fprintf(fp,"</svg>\n");
 fclose(fp);
 printf(" file %s saved \n",filename );

return 0;

}




// ==============================  main ==================================================================================
 


int main()
{
  
  // external rays
  complex double vertices[iPolygonLength]; // array for compute rays function
  char *PolygonPoints = " "; // string for svg

  // critical orbit
  complex double critical_orbit[iOrbitLength];
  char *sCritical = " "; // string for svg
  int r ; // = (yMax - yMin)/600.0; // radius
  
  complex double last_z1, last_z2;


  int q,j; 
  int i;
  double zx, zy;
  double distance;
  


  lldiv_t result; // 
  

  
  c = GiveC(1.0/iPeriodChild, 1.0, 1);
  // 
  alfa = GiveAlfaFixedPoint(c);
  alfaX= creal(alfa);
  alfaY = cimag(alfa);
  
  // viewport for svg file
  iHalfHeight = iHeight/2;
  iHalfWidth = iWidth/2;
  distance = 1.2*GiveDistance(alfaX, alfaY, 0.0, 0.0);
  PixelX = iHalfWidth / distance;
  PixelY = iHalfHeight/distance; 
  PixelWidth = 2.0*distance/iWidth;


  // 
  xMax = alfaX + distance;
  xMin = alfaX - distance;
  yMax = alfaY + distance;
  yMin = alfaY - distance;  
  // r for svg circles  
  r  = iWidth/600; // radius
 



   // vertices = extrnal rays landing  on parabolic fixed point alfa
     ComputeRays( iPeriodChild, IterMax, c , alfa, vertices) ;


   // convert vertices ( array of complex double)   to PolygonPoints ( string ) 
    for (i=0; i<iPolygonLength; i++) {
       zx =  creal(vertices[i]);
       zy = cimag(vertices[i]);
       distance = GiveDistanceInPixels(zx,zy, alfaX, alfaY, PixelWidth);
       if ( distance<DistanceMax) //  clipping 
            asprintf(&PolygonPoints, "%s %d,%d ", PolygonPoints, gx(zx) , gy(zy) );} // 
  
   last_z1 = vertices[iPolygonHalfLength-1]; //zx+zy*I; // save last point on the ray 
   last_z2 = vertices[iPolygonHalfLength+1]; 
  



  // critical orbit 
  result = lldiv (iOrbitLength, iPeriodChild); // periodic orbit
  ComputeOrbit( zcrp, c, critical_orbit);
  // covert array to string for svg
  i = 0;
  for (q = 0; q < result.quot; q++){
    for (j = 0; j < iPeriodChild; j++){
       if (q==0) 
          asprintf(&sCritical, "%s <circle cx=\"%d\" cy=\"%d\" r=\"%d\"/>\n",sCritical, gx(creal(critical_orbit[i])), gy(cimag(critical_orbit[i])), r ); // inverse y axis in svg 
        if (j==0) distance = GiveDistanceInPixels(creal(critical_orbit[i-1]), cimag(critical_orbit[i-1]), creal(critical_orbit[i]), -cimag(critical_orbit[i]), PixelWidth);
                 if ( distance>DistanceStep) 
                    asprintf(&sCritical, "%s <circle cx=\"%d\" cy=\"%d\" r=\"%d\"/>\n",sCritical, gx(creal(critical_orbit[i])), gy(cimag(critical_orbit[i])), r ); // inverse y axis in svg 
           
        
      i+=1; // = q*d +j
   } // for j
  } // for q

  


  
  MakeSVG(iPeriodChild,  PolygonPoints, sCritical , alfa, zcrp, last_z1, last_z2);


  // info 

  // for (i=0; i<iPolygonLength; i++) 
  //  printf(" i = %d z = %f ; %f \n", i, creal(vertices[i]), cimag(vertices[i]));
  //printf("s = %s\n", PolygonPoints);
  printf("c = %f ; %f \n", creal(c), cimag(c));
  printf("alfa = %f ; %f  = %d ; %d\n", alfaX, alfaY, gx(alfaX), gy(alfaY));
  printf("critical point  = %f ; %f  = %d ; %d\n", 0.0, 0.0, gx(0.0), gy(0.0));
  printf("distance from  point %d of the ray and alfa = %.0f in pixels\n", iPolygonHalfLength-1, GiveDistanceInPixels(creal(vertices[iPolygonHalfLength-1]),cimag(vertices[iPolygonHalfLength-1]), alfaX, alfaY, PixelWidth));
  printf("distance from  the last point of critical orbit to alfa = %.0f in pixels\n", GiveDistanceInPixels(creal(critical_orbit[iOrbitLength-1]),cimag(critical_orbit[iOrbitLength-1]), alfaX, alfaY, PixelWidth));
  printf("last_z1 = %f ; %f  = %d ; %d\n", creal(last_z1), cimag(last_z1), gx(creal(last_z1)), gy(cimag(last_z1)));
  printf("last_z2 = %f ; %f  = %d ; %d\n", creal(last_z2), cimag(last_z2), gx(creal(last_z2)), gy(cimag(last_z2)));
  printf(" distance from alfa to critical = %f \n", GiveDistance(alfaX, alfaY, 0.0, 0.0));
  printf(" xMin = %f xMax = %f yMin = %f yMax = %f \n", xMin, xMax, yMin, yMax);
  printf(" PixelWidth = %f  PixelX = %f PixelY = %f \n",PixelWidth,  PixelX, PixelY);
  printf(" iWidth = %d iHalfWidth = %d iHeight = %d iHalfHeight = %d\n", iWidth, iHalfWidth, iHeight, iHalfHeight);
  printf(" iPeriodChild = %d iOrbitLength = %d IterMax = %lld \n",iPeriodChild, iOrbitLength, IterMax);

  

  return 0;
}

